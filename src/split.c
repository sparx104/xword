#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//
// Crossword solver
// Copyright (C) 2017-18 by Chris Dickens
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

// !!! .main
int main(
    int argc,
    char **argv
) {
    (void) argc;
    (void) argv;
    //
    FILE *f;
    f = fopen( "words.txt", "r" );
    if( !f ) {
        abort();
    }
    //
    // max word length in the file is 33
    char buf[256];
    FILE *out[34];
    int i;
    for( i = 1; i < 34; i++ ) {
        snprintf( buf, 256, "words/%d.txt", i );
        out[i] = fopen( buf, "w" );
    }
    //
    size_t len;
    char *nl;
//    size_t maxlen = 0;
    while( fgets( buf, 256, f ) ) {
        nl = strchr( buf, '\n' );
        if( nl ) {
            *nl = '\0';
        }
        nl = strchr( buf, '\r' );
        if( nl ) {
            *nl = '\0';
        }
        //
        len = strlen( buf );
        if( len ) {
            fprintf( out[(int)len], "%s\n", buf );
        }
    }
    //
//    printf( "max length: %zu\n", maxlen );
    //
    for( i = 1; i < 34; i++ ) {
        fclose( out[i] );
    }
    //
    fclose( f );
    return 0;
}