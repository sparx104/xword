//
// Crossword Solver
// Copyright (C) 2017-18 by Chris Dickens
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// "words.txt" datafile is from
//   https://github.com/dwyl/english-words/blob/master/words_alpha.txt
//
// Use split.c to generate the files in words/
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int anagram( const char *, const char * );
static int crossword( const char *, const char * );

// !!! .main
int main(
    int argc,
    char **argv
) {
    char *wordlist_path;
    if( argc < 2 ) {
        printf( "Use: %s <mode> <pattern> [<wordlist>]\n", argv[0] );
        printf( "     <mode> is 'a' for anagrams, 'w' for cross-word\n" );
        printf( "     <pattern> is match pattern, use '-' for unknowns\n" );
        printf( "     <wordlist> is the word list to use.  Supports plain\n" );
        printf( "       and compressed lists.  Default is 'words.cmp'\n" );
        return 2;
    }
    if( argc > 2 ) {
        wordlist_path = argv[3];
    } else {
        wordlist_path = NULL;
    }
    //
    int (*lookup)( const char *, const char * );
    switch( *(argv[1]) ) {
        case 'w':
        case 'W':
            lookup = &crossword;
            break;
            //
        case 'a':
        case 'A':
            lookup = &anagram;
            break;
            //
        default:
            printf( "Use 'a' or 'w' for mode\n" );
            return 1;
    }
    //
    char buf[256];
    char pattern[256], c;
    size_t len, i;
    FILE *words;
    //
    // copy and lcase the pattern
    i = 0;
    len = strlen( argv[2] );
    if( (len < 1) || (len > 255) ) {
        // invalid length - no words
        return 0;
    }
    //
    for( i = 0; i < len; i++ ) {
        c = argv[2][i];
        if( (c >= 'A') && (c <= 'Z') ) {
            c += 32;
        }
        pattern[i] = c;
    }
    pattern[i] = '\0';
    //
    // Open wordlist
    if( !wordlist_path ) {
        wordlist_path = "words.cmp";
    }
    words = fopen( wordlist_path, "r" );
    if( !words ) {
        // invalid file
        fprintf( stderr, "Unable to read %s\n", wordlist_path );
        return 1;
    }
    //
    // Decompressor init
    const char *cmp = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char decmp[64], last[64], *cmp_p;
    int cmp_l;
    memset( decmp, '\0', 64 );
    //
    // Read each line, remove the \n and pass to lookup function
    char *word;
    size_t patt_len;
    patt_len = strlen( pattern );
    while( fgets( buf, 256, words ) != NULL ) {
        buf[255] = '\0';
        //
        // Decompress
        cmp_p = strchr( cmp, buf[0] );
        if( cmp_p ) {
            cmp_l = (cmp_p - cmp);
            memcpy( decmp, last, cmp_l );
            strncpy( (decmp + cmp_l), (buf + 1), (64 - cmp_l) );
            decmp[63] = '\0';
            word = decmp;
            len = strlen( word );
//            printf( "%s %d %s\n", buf, cmp_l, word );
//            break;
        } else {
            word = buf;
        }
        //
        // remove any newline (\n only)
        len = strlen( word );
        if( word[len - 1] == '\n' ) {
            len -= 1;
            word[len] = '\0';
        }
        //
        if( len == patt_len ) {
            if( (*lookup)( word, pattern ) ) {
                printf( "%s\n", word );
            }
        }
        //
        memcpy( last, word, len );
        last[len] = '\0';
    }
    //
    fclose( words );
    return 0;
}

// !!! .crossword
// Solve crossword clue, PATTERN should contain letters and anything
// not A-Z/a-z will be used as 'unknown'
//
// PATTERN must be in lower case
//
// RETURNS: 0 on success, something else on error
//
static int crossword(
    const char *word,
    const char *pattern
) {
    //
    // "line" is the same length as our word the files are
    // split that way.  
    while( *pattern ) {
        if( (*pattern >= 'a') && (*pattern <= 'z') ) {
            // check the char
            if( *pattern != *word ) {
                return 0;
            }
        }
        //
        pattern += 1;
        word += 1;
    }
    return 1;
}

// !!! .anagram
// Lookup PATTERN as an anagram
//
static int anagram(
    const char *word,
    const char *pattern
) {
static char buf[256];
    //
    // We need a copy because we're going to modify it
    strncpy( buf, word, 256 );
    buf[255] = '\0';
    //
    char *found;
    while( *pattern ) {
        if( *pattern == '-' ) {
            // any character
            pattern += 1;
            continue;
        }
        //
        found = strchr( buf, *(pattern++) );
        if( found ) {
            // remove the found char so it won't be matched again
            *found = '-';
        } else {
            return 0;
        }
    }
    return 1;
}
