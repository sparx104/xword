CC=gcc
CFLAGS = -Wall -Wextra -O3

all: xword

clean:
	rm -f *.o
	rm -f xword

xword: xword.o
	$(CC) $(CFLAGS) -o $@ $^

xword.o: src/xword.c
	$(CC) $(CFLAGS) -c $<
