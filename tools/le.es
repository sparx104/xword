&use filereader, file, rt

//
// Crossword Solver
// Copyright (C) 2017-18 by Chris Dickens
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

task "main" () {
    !filereader r
//    r = new( "cmptest.txt" );
    r = new( "words_top_300k.txt" );
    if_not( r ) {
        rt.fail( 1, "Unable to read" );
    }endif
    //
    do {
        !bool ok
        !string word
        ok, word = r.read_line();
        done_if_not( ok );
        //
        word = word.trim();
        println( word );
    }loop
    //
    r.close();
}endtask
