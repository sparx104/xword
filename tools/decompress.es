#!/usr/bin/evolve

//
// Crossword Solver
// Copyright (C) 2017-18 by Chris Dickens
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

&use filereader, file, rt

task "main" () {
    !string src
    src = rt.get_arg( "_" );
    if_not( src ) {
        println( "Use: decompress.es FILE" );
        rt.fail( 2 );
    }endif
    //
    !filereader r
    r = new( src );
    if_not( r ) {
        rt.fail( 1, "Unable to read" );
    }endif
    //
    !string last, cmp
    last = string.new();
    cmp = string.new( "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
    do {
        !bool ok
        !string word
        ok, word = r.read_line();
        done_if_not( ok );
        //
        !string lc, pfx
        !bool found
        !int cmp_l
        lc, word = word.split_at( 1 );
        cmp_l, found = cmp.find_first( lc );
        if( found ) {
            pfx, * = last.split_at( cmp_l );
            word = pfx.append( word );
        } else {
            word = lc.append( word );
        }endif
        println( word );
        //
        last = word.dup();
    }loop
    r.close();
}endtask
