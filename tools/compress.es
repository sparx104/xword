#!/usr/bin/evolve

//
// Crossword Solver
// Copyright (C) 2017-18 by Chris Dickens
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

&use filereader, file, rt

task "main" () {
    !int max_len
    !string in
    max_len = rt.get_arg( "s", "-max-size" );
    if_not( max_len ) {
        max_len = int.new( 99 );
    }endif
    in = rt.get_arg( "_" );
    if_not( in ) {
        println( "Use: compress.es FILE [-s|-max-size MAX_LENGTH]" );
        rt.fail( 2 );
    }endif
    //
    !filereader r
    r = filereader.new( in );
    if_not( r ) {
        rt.fail( 1, "Unable to read" );
    }endif
    //
    !string last, cmp
    last = string.new();
    cmp = string.new( "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
    do {
        !bool ok
        !string word
        ok, word = r.read_line();
        done_if_not( ok );
        //
        !int l
        !bool too_long
        l = word.length();
        too_long = l.gte( max_len );
        if( too_long ) {
            redo;
        }endif
        //
        !int pl
        !bool worth_it
        pl = prefix_count( word, last );
        if( pl ) {
            !string suf, cs
            *, suf = word.split_at( pl );
            cs = cmp.crop( pl, 1 );
            println( cs, suf );
        } else {
            println( word );
        }endif
        //
        last = word.dup();
    }loop
    //
    r.close();
}endtask

// !!! .prefix_count
// Count the number of chars that match in the prefix
task "prefix_count" ( "a", "b" ) {
    !string a, b
    !int c
    c = int.new( 0 );
    do {
        !string pa, pb
        pa, a = a.split_at( 1 );
        pb, b = b.split_at( 1 );
        if_not( pa ) {
            done;
        }endif
        if_not( pb ) {
            done;
        }endif
        //
        !bool m
        m = pa.eq( pb );
        if_not( m ) {
            done;
        }endif
        //
        c = c.add( 1 );
    }loop
    //
    return c;
}endtask
    
    